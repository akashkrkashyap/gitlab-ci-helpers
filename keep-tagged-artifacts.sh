#!/bin/bash

VARS=( KEP_BUILD_ID KEEP_TAG_REGEX )

. <(curl -fksSL https://gitlab.com/akashkrkashyap/gitlab-ci-helpers/raw/master/check-vars.sh)

if [[ "$CI_BUILD_TAG" =~ ^($KEEP_TAG_REGEX) ]]; then
  echo "Alright, will keep build #$KEEP_BUILD_ID"
  curl -s --request POST \
  --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
  "$(echo $CI_PROJECT_URL |  cut -d'/' -f1-3)/api/v4/projects/$CI_PROJECT_ID/jobs/$KEEP_BUILD_ID/artifacts/keep" \
  | python -m json.tool
fi
